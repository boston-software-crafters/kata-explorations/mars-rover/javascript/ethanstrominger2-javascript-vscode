import { Map, execute } from './Foo.js'
// -- execute("MMRMMLM") -> "2:3:N"
describe('Mars Rover Test', () => {
  it('You can create a map', () => {
    const map = new Map();
  });

  it('It should move one step to the N', () => {
    expect(execute('M')).toEqual('0:1:N');
  });

  it('It should face E', () => {
    expect(execute('R')).toEqual('0:0:E');
  });

  it('It should face S', () => {
    expect(execute('RR')).toEqual('0:0:S');
  });
  it('It should face W', () => {
    expect(execute('L')).toEqual('0:0:W');
  });
  it('It should move two steps to the N', () => {
    expect(execute('MM')).toEqual('0:2:N');
  });
});
