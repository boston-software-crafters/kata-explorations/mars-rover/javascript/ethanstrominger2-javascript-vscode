class Map {}

function execute(directions) {
  const x = 0;
  let y = 0;
  let direction = 'N';
  directions.split('').forEach(move => {
    if (move === 'M') {
      y += 1;
    }
  });

  directions.split('').forEach(move => {
    if (move === 'R') {
      if (direction === 'N') {
        direction = 'E';
      } else if (direction === 'E') {
        direction = 'S';
      }
    }
  });

  if (directions === 'R') {
    direction = 'E';
  } else if (directions === 'L') {
    direction = 'W';
  }
  return `${x}:${y}:${direction}`;
}

export { Map, execute };
